proto-gen:
	./scripts/gen-proto.sh  ${CURRENT_DIR}
	rm -rf vendor/genproto
	sudo rm -rf ${GOROOT}/src/genproto
	sudo cp -R genproto ${GOROOT}/src
	mv genproto vendor

pull-proto-module:
	git submodule update --init --recursive

update-proto-module:
	git submodule update --remote --merge